Name:           perl-IPC-Cmd
Epoch:          2
Version:        1.04
Release:        5
Summary:        Finding and running system commands made easy
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/IPC-Cmd
Source0:        https://cpan.metacpan.org/authors/id/B/BI/BINGOS/IPC-Cmd-%{version}.tar.gz
BuildArch:      noarch
#For build:
BuildRequires:  make perl-generators perl-interpreter perl(ExtUtils::MakeMaker) >= 6.76
# For run:
BuildRequires:  perl(Carp) perl(constant) perl(Exporter) perl(File::Spec) perl(Text::ParseWords)
BuildRequires:  perl(FileHandle) perl(IO::Handle) perl(IO::Select) perl(IPC::Open3)
BuildRequires:  perl(Locale::Maketext::Simple) perl(Module::Load::Conditional) >= 0.66
BuildRequires:  perl(Params::Check) >= 0.20 perl(POSIX) perl(Socket) perl(strict) perl(Symbol)
BuildRequires:  perl(Time::HiRes) perl(vars)
#For test:
BuildRequires:  perl(Data::Dumper) perl(File::Temp) perl(lib) perl(Test::More) perl(warnings)
# Dependencies:
Requires:       perl(ExtUtils::MakeMaker)
Requires:       perl(FileHandle)
Requires:       perl(IO::Handle)
Requires:       perl(IO::Select)
Requires:       perl(IPC::Open3)
Suggests:       perl(IPC::Run) >= 0.55
Requires:       perl(Module::Load::Conditional) >= 0.66
Requires:       perl(Params::Check) >= 0.20
Requires:       perl(POSIX)
Requires:       perl(Socket)
Requires:       perl(Time::HiRes)

%description
IPC::Cmd allows for the searching and execution of any binary on your system.
It adheres to verbosity settings and is able to run intereactive.
It also has an option to capture output/error buffers.

%package_help

%prep
%autosetup -n IPC-Cmd-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}

%check
make test

%files
%doc CHANGES README
%{perl_vendorlib}/IPC/

%files help
%{_mandir}/man3/IPC::Cmd.3*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 2:1.04-5
- drop useless perl(:MODULE_COMPAT) requirement

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:1.04-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded build require

* Fri Jan 3 2020 openEuler Buildteam <buildteam@openeuler.org> - 2:1.04-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete redundant file

* Mon Oct 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:1.04-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete unneeded build requires of perl(IPC::Run)

* Wed Sep 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:1.04-1
- Package init
